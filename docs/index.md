# Электронный атлас проводящих путей головного мозга человека

Исходные коды [git](https://git.miem.hse.ru/19218/cpp_converter.git).

## Используемые библиотеки

	#include <iostream>
	#include <fstream>
	#include <sstream>
	#include <string>
	#include <vector>
	#include <filesystem>
	#include "windows.h"
	#include <chrono>
	#include <draco/mesh/mesh.h>
	#include <draco/compression/encode.h>
	#include <draco/mesh/triangle_soup_mesh_builder.h>



## Опции для энкодинга Draco Mesh
	struct EncodingOptions
	{
    		int encoding_method;    // метод кодировки (sequential/edgebreaker и т.д.)

    		int encoding_speed;     // скорость кодирования
    		int decoding_speed;     // скорость декодирования

    		// quantization bits
    		int position_qb;        // для вершин
    		int normal_qb;          // для нормалей
    		int tex_coord_qb;       // для текстурных координат
    		int color_qb;           // для цвета
    		int generic_qb;         // для универсального атрибута
	};

## Нахождение обртаной и транспонированной матрицы, запись результата в Res

	void inverse_and_transpose_4x4(const float M[4][4], float result[4][4])
	{
    	inverse_4x4(M, result);
   		transpose_4x4(result);
	}

### Нахождение обратной матрицы 4х4

	void inverse_4x4(const float M[4][4], float result[4][4])
	{
    		// считаем миноры матрицы
    		float subdet1 = M[0][0] * (M[1][1] * (M[2][2] * M[3][3] - M[3][2] * M[2][3]) - M[2][1] * (M[1][2] * M[3][3] - M[3][2] * M[1][3]) + M[3][1] * (M[1][2] * M[2][3] - M[2][2] * M[1][3]));
    		float subdet2 = M[1][0] * (M[0][1] * (M[2][2] * M[3][3] - M[3][2] * M[2][3]) - M[2][1] * (M[0][2] * M[3][3] - M[3][2] * M[0][3]) + M[3][1] * (M[0][2] * M[2][3] - M[2][2] * M[0][3]));
    		float subdet3 = M[2][0] * (M[0][1] * (M[1][2] * M[3][3] - M[3][2] * M[1][3]) - M[1][1] * (M[0][2] * M[3][3] - M[3][2] * M[0][3]) + M[3][1] * (M[0][2] * M[1][3] - M[1][2] * M[0][3]));
    		float subdet4 = M[3][0] * (M[0][1] * (M[1][2] * M[2][3] - M[2][2] * M[1][3]) - M[1][1] * (M[0][2] * M[2][3] - M[2][2] * M[0][3]) + M[2][1] * (M[0][2] * M[1][3] - M[1][2] * M[0][3]));

    		// получаем детерминант
    		float determinant = subdet1 - subdet2 + subdet3 - subdet4;
    		if (determinant == 0) {
        	std::cout << "Inverse does not exist" << std::endl;
        	return;
    		}

    		// записываем полученные элементы матрицы
    	for (int i = 0; i < 4; ++i) {
        	for (int j = 0; j < 4; ++j) {
            	int sign = ((i + j) % 2) ? -1 : 1;
            	result[i][j] = sign * (
                    (M[(j + 1) % 4][(i + 1) % 4] * M[(j + 2) % 4][(i + 2) % 4] * M[(j + 3) % 4][(i + 3) % 4]) + 
                    (M[(j + 1) % 4][(i + 2) % 4] * M[(j + 2) % 4][(i + 3) % 4] * M[(j + 3) % 4][(i + 1) % 4]) + 
                    (M[(j + 1) % 4][(i + 3) % 4] * M[(j + 2) % 4][(i + 1) % 4] * M[(j + 3) % 4][(i + 2) % 4]) - 
                    (M[(j + 1) % 4][(i + 3) % 4] * M[(j + 2) % 4][(i + 2) % 4] * M[(j + 3) % 4][(i + 1) % 4]) - 
                    (M[(j + 1) % 4][(i + 2) % 4] * M[(j + 2) % 4][(i + 1) % 4] * M[(j + 3) % 4][(i + 3) % 4]) - 
                    (M[(j + 1) % 4][(i + 1) % 4] * M[(j + 2) % 4][(i + 3) % 4] * M[(j + 3) % 4][(i + 2) % 4])
                ) / determinant;
        	}
    	}
	}

### Транспонирование 4x4 матрици M in-place
	void transpose_4x4(float M[4][4])
	{
    		for (int i = 0; i < 4; i++)
        		for (int j = i + 1; j < 4; j++)
            		std::swap(M[i][j], M[j][i]);
	}


## Применяет заданные опции encoding_options к кодировщику encoder
	void apply_encoding_options(draco::Encoder* encoder, const EncodingOptions& encoding_options)
	{
    	encoder->SetSpeedOptions(encoding_options.encoding_speed, encoding_options.decoding_speed);
    	encoder->SetAttributeQuantization(draco::GeometryAttribute::POSITION, encoding_options.position_qb);    // default: 11
    	encoder->SetAttributeQuantization(draco::GeometryAttribute::NORMAL, encoding_options.normal_qb);        // default: 7
    	encoder->SetAttributeQuantization(draco::GeometryAttribute::TEX_COORD, encoding_options.tex_coord_qb);  // default: 10
    	encoder->SetAttributeQuantization(draco::GeometryAttribute::COLOR, encoding_options.color_qb);          // default: 8
    	encoder->SetAttributeQuantization(draco::GeometryAttribute::GENERIC, encoding_options.generic_qb);      // default: 8
    	encoder->SetEncodingMethod(encoding_options.encoding_method);
	}

## Конвертация одного nrtl мэша в формат draco с указанными опциями encoding_options
	std::shared_ptr<draco::Mesh> NRTLMeshToDraco(const nrtl::Mesh& nrtl_mesh, const EncodingOptions& encoding_options, const std::string& output_file_prefix = "nrtl_draco_mesh_")
	{
    	// определеяем, является ли матрица трансформации единичной
    	bool identity_matrix = true;
    	for (int i = 0; i < 4; ++i) {
        	if (nrtl_mesh.model_matrix[i][i] != 1) {
            	identity_matrix = false;
            	break;
        	}
        	for (int j = 0; j < 4; ++j) {
            	if (i != j && nrtl_mesh.model_matrix[i][j] != 0) {
                	identity_matrix = false;
                	break;
            	}
        	}
    	}

    	// получаем информацию из nrtl мэша
    	MeshData mesh_data = nrtl_mesh.data;
    	float* mesh_vertices = mesh_data.vertices;
    	float* mesh_texCoords = mesh_data.texCoords;
    	float* mesh_normals = mesh_data.normals;
    	uint32_t* mesh_indices = mesh_data.indices;

    	int VERTICES_COUNT = mesh_data.verticesCount;
    	int NORMALS_COUNT = mesh_data.normalsCount;
    	int INDICES_COUNT = mesh_data.indicesCount;
    	int TEX_COORDS_COUNT = mesh_data.texCoordsCount;

    	std::cout << "VERTICES_COUNT: " << VERTICES_COUNT << std::endl;
    	std::cout << "NORMALS_COUNT: " << NORMALS_COUNT << std::endl;
    	std::cout << "INDICES_COUNT: " << INDICES_COUNT << std::endl;
    	std::cout << "TEX_COORDS_COUNT: " << TEX_COORDS_COUNT << std::endl;

    	//for (int i = 0; i < TEX_COORDS_COUNT; ++i) {
        	//cout << mesh_texCoords[i] << " ";
    	//}

    	// создаем объект драко мэша, устанавливаем количество вершин
    	std::shared_ptr<draco::Mesh> draco_mesh = std::make_shared<draco::Mesh>();
    	draco_mesh->draco::Mesh::set_num_points(VERTICES_COUNT / 3);

    	// добавляем атрибуты вершин и нормалей
    	draco::GeometryAttribute pa;
    	pa.Init(draco::GeometryAttribute::POSITION, nullptr, 3, draco::DT_FLOAT32, false, sizeof(float) * 3, 0);
    	int32_t pos_att_id = draco_mesh->AddAttribute(pa, true, VERTICES_COUNT / 3);

    	draco::GeometryAttribute na;
    	na.Init(draco::GeometryAttribute::NORMAL, nullptr, 3, draco::DT_FLOAT32, false, sizeof(float) * 3, 0);
    	int32_t norm_att_id = draco_mesh->AddAttribute(na, true, NORMALS_COUNT / 3);

    	// для изменения цвета мэша
    	/*
    	draco::GeometryAttribute ca;
    	ca.Init(draco::GeometryAttribute::COLOR, nullptr, 3, draco::DT_UINT8, false, sizeof(uint8_t) * 3, 0);
    	int32_t color_att_id = draco_mesh->AddAttribute(ca, true, VERTICES_COUNT / 3);
    	*/

	#ifdef DEBUG_MODEL_MATRIX
    	std::cout << "\nModel matrix:\n";
    	for (int i = 0; i < 4; ++i) {
        	for (int j = 0; j < 4; ++j) {
            	std::cout << nrtl_mesh.model_matrix[j][i] << ' ';
        	}
        	std::cout << "\n";
    	}
	#endif

    	// получаем матрицу (M^-1)^T
    	float inverse_model_matrix[4][4];
    	inverse_and_transpose_4x4(nrtl_mesh.model_matrix, inverse_model_matrix);

	#ifdef DEBUG_MODEL_MATRIX
    	std::cout << "\nTranspose of inverse model matrix:\n";
    	for (int i = 0; i < 4; ++i) {
        	for (int j = 0; j < 4; ++j) {
            	std::cout << inverse_model_matrix[j][i] << ' ';
        	}
        	std::cout << "\n";
    	}
	#endif

    	// установка дефолтного цвета
    	//std::array<uint8_t, 3> default_color = { 229, 229, 229 };
    
    	// добавляем значения для созданных атрибутов
    	for (int i = 0; i < VERTICES_COUNT; i += 3) {
        	// текущая вершина и нормаль
        	std::array<float, 3> vertex = { mesh_vertices[i], mesh_vertices[i + 1], mesh_vertices[i + 2] };
        	std::array<float, 3> normal = { mesh_normals[i], mesh_normals[i + 1], mesh_normals[i + 2] };

        	// если матрица не единичная, то применяем трансформацию для вершин
        	if (!identity_matrix) {
            	std::array<float, 4> homogeneous_vertex = { vertex[0], vertex[1], vertex[2], 1 };
            	std::array<float, 4> result_vertex = { 0, 0, 0, 0 };
            	for (int j = 0; j < 4; ++j)
                	for (int k = 0; k < 4; ++k)
                    	result_vertex[j] += nrtl_mesh.model_matrix[k][j] * homogeneous_vertex[k];

            	for (int l = 0; l < 3; ++l)
                	vertex[l] = result_vertex[l] / result_vertex[3];
        	}

        	// recheck for correctness
        	// если матрица не единичная, то применяем трансформацию для нормалей
        	if (!identity_matrix) {
            	std::array<float, 4> homogeneous_normal = { normal[0], normal[1], normal[2], 0 };
            	std::array<float, 4> result_normal = { 0, 0, 0, 0 };

            	for (int j = 0; j < 4; ++j)
                	for (int k = 0; k < 4; ++k)
                    	result_normal[j] += inverse_model_matrix[k][j] * homogeneous_normal[k];

            	for (int l = 0; l < 3; ++l)
                	normal[l] = result_normal[l]; // should be unit vec, but better add normalization
        	}

        	// устанавливаем значения для текущей нормали и вершины в драко мэше
        	draco_mesh->attribute(pos_att_id)->SetAttributeValue(draco::AttributeValueIndex(i / 3), &vertex);
        	draco_mesh->attribute(norm_att_id)->SetAttributeValue(draco::AttributeValueIndex(i / 3), &normal);
        	// для установки цвета необходимо расскоментировать создание атрибута GeometryAttribute::COLOR
        	//draco_mesh->attribute(color_att_id)->SetAttributeValue(draco::AttributeValueIndex(i / 3), &default_color);
    	}


    	// добавляем грани
    	for (draco::FaceIndex i(0); i < INDICES_COUNT / 3; i++) {
        	draco::Mesh::Face face;
        	for (int j = 0; j < 3; j++) {
            	face[j] = mesh_indices[3 * i.value() + j];
        	}
        	draco_mesh->draco::Mesh::SetFace(i, face);
    	}

    	// создаем объект кодировщика, применяем переданные опции и сохраняем файл
    	draco::Encoder encoder;
    	apply_encoding_options(&encoder, encoding_options);
    	draco::EncoderBuffer buffer;
    	encoder.EncodeMeshToBuffer(*draco_mesh, &buffer);
    	std::ofstream out_file(output_file_prefix + ".drc", std::ios::binary);
    	out_file.write(buffer.data(), buffer.size());
    	return draco_mesh;
	}

## Конвертация всех объектов сцены в драко мэши, извлекая тракты в отдельные мэши
	
	//Использует encoding_options в качестве параметров кодировщика в обоих случаях
	void NRTLSceneToDraco(Scene* sc, const EncodingOptions& encoding_options, const std::string output_file_prefix = "nrtl_draco_")
	{
    	// получаем все мэши и общую информацию сцены
    	Mesh* nrtl_meshes = sc->meshes;
    	uint32_t MESH_COUNT = sc->meshCount;
    	uint32_t TRACT_COUNT = sc->tractCount;
    	uint32_t STEP_COUNT = sc->stepCount;
    	uint32_t SECTION_COUNT = sc->sectionCount;
    
    	// создаем список драко мэшей (без извлеченных трактов)
    	std::vector< std::shared_ptr<draco::Mesh> > nrtl_and_draco_vec;
    	for (int i = 0; i < MESH_COUNT; ++i)
        	nrtl_and_draco_vec.push_back(NRTLMeshToDraco(nrtl_meshes[i], encoding_options, output_file_prefix + "mesh_" + std::to_string(i)));


    	Tract* tracts = sc->tracts;
    	Section* sections = sc->sections;


    	// извлекаем тракты
    	for (int i = 0; i < TRACT_COUNT; ++i) {
        	Tract tract = tracts[i];
        	Rgb tract_color = tract.color;

        	// получаем цвет тракта
        	std::array<uint8_t, 3> rgb = { tract_color.r, tract_color.g, tract_color.b };
        	//std::cout << "tract.color: " << int(tract_color.r) << ' ' << int(tract_color.g) << ' ' << int(tract_color.b) << std::endl;

        	// число секций, соответствующих текущему тракту
        	int TRACT_SECTION_COUNT = tract.sectionCount;
        	for (int section_i = 0; section_i < TRACT_SECTION_COUNT; ++section_i) {
            	Section cur_section = sections[tract.sectionIds[section_i]];
            	Mesh cur_mesh = nrtl_meshes[cur_section.meshCutId];
            	SubMesh cur_submesh = cur_mesh.subMeshes[cur_section.subMeshId];
            
            	uint32_t* sub_indices = cur_submesh.subIndices;
            	int SUB_INDEX_COUNT = cur_submesh.subIndexCount;

            	//cout << "SUB_INDEX_COUNT: " << SUB_INDEX_COUNT << endl;
            	//for (int i = 0; i < SUB_INDEX_COUNT; ++i) {
            	//    cout << sub_indices[i] << endl;;
            	//}
            	//cout << endl;

            	// кодирование драко в данном случае удобно делать через TriangleSoupMeshBuilder
            	// сразу после создания указываем количество граней
            	draco::TriangleSoupMeshBuilder mb;
            	mb.Start(SUB_INDEX_COUNT);

            	// добавляем атрибуты
            	const int tract_pos_att_id =
                	mb.AddAttribute(draco::GeometryAttribute::POSITION, 3, draco::DT_FLOAT32);

            	const int tract_norm_att_id =
                	mb.AddAttribute(draco::GeometryAttribute::NORMAL, 3, draco::DT_FLOAT32);

            	const int tract_color_att_id =
                	mb.AddAttribute(draco::GeometryAttribute::COLOR, 3, draco::DT_UINT8);


            	// получаем уже существующий драко мэш для текущего nrtl мэша
            	std::shared_ptr<draco::Mesh> draco_mesh = nrtl_and_draco_vec[cur_section.meshCutId];
            	uint32_t* cur_mesh_indices = cur_mesh.data.indices;
            	int CUR_MESH_INDICES_COUNT = cur_mesh.data.indicesCount;
            	for (int i = 0; i < SUB_INDEX_COUNT; ++i) {
                	// атрибуты мэшей трактов являются подмножествами основных мэшей,
                	// поэтому значения для вершин и нормалей берем из текущего существующего мэша

                	// attr_id == 0 соответствует вершинам
                	auto vertex_1 = draco_mesh->attribute(0)->GetValue<float, 3>(draco::AttributeValueIndex(cur_mesh_indices[3 * sub_indices[i]]));
                	auto vertex_2 = draco_mesh->attribute(0)->GetValue<float, 3>(draco::AttributeValueIndex(cur_mesh_indices[3 * sub_indices[i] + 1]));
                	auto vertex_3 = draco_mesh->attribute(0)->GetValue<float, 3>(draco::AttributeValueIndex(cur_mesh_indices[3 * sub_indices[i] + 2]));

                	// attr_id == 1 соответствует нормалям
                	auto normal_1 = draco_mesh->attribute(1)->GetValue<float, 3>(draco::AttributeValueIndex(cur_mesh_indices[3 * sub_indices[i]]));
                	auto normal_2 = draco_mesh->attribute(1)->GetValue<float, 3>(draco::AttributeValueIndex(cur_mesh_indices[3 * sub_indices[i] + 1]));
                	auto normal_3 = draco_mesh->attribute(1)->GetValue<float, 3>(draco::AttributeValueIndex(cur_mesh_indices[3 * sub_indices[i] + 2]));

                	// устанавливаем значения
                	mb.SetAttributeValuesForFace(tract_pos_att_id, draco::FaceIndex(i), &vertex_1, &vertex_2, &vertex_3);
                	mb.SetAttributeValuesForFace(tract_norm_att_id, draco::FaceIndex(i), &normal_1, &normal_2, &normal_3);
                	mb.SetAttributeValuesForFace(tract_color_att_id, draco::FaceIndex(i), &rgb, &rgb, &rgb);
            	}

            	// получаем созданный драко мэш через метод Finalize
            	// устанавливаем переданные опции кодировщику и создаем мэш
            	std::shared_ptr<draco::Mesh> tract_mesh = mb.Finalize();
            	draco::Encoder tract_encoder;
            	apply_encoding_options(&tract_encoder, encoding_options);
            	draco::EncoderBuffer tract_buffer;
            	tract_encoder.EncodeMeshToBuffer(*tract_mesh, &tract_buffer);
            	// в названии указываем, к какому мэшу и секциии относится текущая часть тракта
            	std::ofstream tract_out_file(output_file_prefix + "mesh_" + std::to_string(cur_section.meshCutId) + "_section_" + std::to_string(tract.sectionIds[section_i]) + "_tract_" + std::to_string(cur_section.tractId) + ".drc", std::ios::binary);
            	tract_out_file.write(tract_buffer.data(), tract_buffer.size());
        	}
    	}
	}

### 

	void ConvertAllToDraco(const std::string& path_to_nrtl_objects, const EncodingOptions& encoding_options)
	{
    	for (const auto& dir_entry : std::filesystem::directory_iterator(path_to_nrtl_objects)) {
        	std::filesystem::path path = dir_entry.path();
        	std::string file_name = path.filename().string();
        	size_t extension_index = file_name.find_last_of('.');

        	std::string raw_file_name = file_name.substr(0, extension_index);
        	std::string extension = file_name.substr(extension_index + 1);
        	if (extension != "nrtl")
            	continue;
        	if (!(std::filesystem::exists(raw_file_name) && std::filesystem::is_directory(raw_file_name)) && !std::filesystem::create_directory(raw_file_name))
            	continue;

        	Scene* sc = readFile(path.string().c_str());
        	NRTLSceneToDraco(sc, encoding_options, raw_file_name + "/" + raw_file_name + "_");
    	}
	}

## Функция main

	void main(int argc, char* argv[])
	{

	...

		EncodingOptions encoding_options;
    	encoding_options.encoding_speed   = 10;
    	encoding_options.decoding_speed   = 10;
    	encoding_options.position_qb      = 15;
    	encoding_options.normal_qb        = 15;
    	encoding_options.tex_coord_qb     = 15;
    	encoding_options.color_qb         = 15;
    	encoding_options.generic_qb       = 15;
    	encoding_options.encoding_method  = draco::MESH_EDGEBREAKER_ENCODING;

    	auto start = std::chrono::steady_clock::now();
    	ConvertAllToDraco(argv[2], encoding_options);
    	auto end = std::chrono::steady_clock::now();
    	std::cout << "\nTime taken: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " msec" << std::endl;
	
	...
	}

## Возможность задавать параметры в командной строке

    void main(int argc, char* argv[]){

		...

        	fs::create_directory(argv[2]); // создание папки для хранения полученных моделей
		
		...

        	Scene* sc = readFile(argv[1]); // возможность считывать параметр, который задан в командной строке
		
		...

	}